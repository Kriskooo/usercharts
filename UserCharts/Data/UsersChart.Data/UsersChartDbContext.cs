﻿using Microsoft.EntityFrameworkCore;
using UsersChart.Data.Models;

namespace UsersChart.Data;

public class UsersChartDbContext : DbContext
{
    public UsersChartDbContext(DbContextOptions<UsersChartDbContext> options)
        : base(options)
    {
    }

    public DbSet<User> Users { get; set; }
    
    public DbSet<Project> Projects { get; set; }
    
    public DbSet<TimeLog> TimeLogs { get; set; }
    
    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        => SaveChangesAsync(true, cancellationToken);
}