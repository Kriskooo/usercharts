namespace UserChart.Client.Models;

public class TimeLogRequestModel
{
    public int Page { get; set; }
    public DateTime? From { get; set; }
    public DateTime? To { get; set; }
}