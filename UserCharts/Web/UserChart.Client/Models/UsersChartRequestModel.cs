namespace UserChart.Client.Models;

public class UsersChartRequestModel
{
    public string SelectedOption { get; set; }
    public DateTime? From { get; set; }
    public DateTime? To { get; set; }
}