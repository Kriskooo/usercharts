namespace UserChart.Business.InitializeDatabase;

public interface IInitializeDatabaseService
{
    Task InitializeDatabase();
}