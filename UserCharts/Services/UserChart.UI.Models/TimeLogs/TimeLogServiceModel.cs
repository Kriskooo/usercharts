namespace UserChart.UI.Models.TimeLogs;

public class TimeLogServiceModel
{
    public int Page { get; set; }
    public DateTime? From { get; set; }
    public DateTime? To { get; set; }
}