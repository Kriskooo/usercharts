﻿
namespace UserChart.UI.Models.TimeLogs;

public class TimeLogsListingModel
{
    public string Id { get; set; }
    public string Username { get; set; }
    public string Email { get; set; }
    public string ProjectName { get; set; }
    public DateTime Date { get; set; }
    public float HoursWorked { get; set; }

}