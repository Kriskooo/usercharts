namespace UserChart.UI.Models.TimeLogs;

public class TimeLogUserModel
{
    public string Username { get; set; }
    public float HoursWorked { get; set; }
}