namespace UserChart.UI.Models.UserCharts;

public class UsersChartServiceModel
{
    public string SelectedOption { get; set; }
    public DateTime? From { get; set; }
    public DateTime? To { get; set; }
}