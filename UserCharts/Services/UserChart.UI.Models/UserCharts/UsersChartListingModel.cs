namespace UserChart.UI.Models.UserCharts;

public class UsersChartListingModel
{
    public string KeyNameValue { get; set; }
    public float HoursWorked { get; set; }
}