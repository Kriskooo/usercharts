# UserCharts

<p>ASP.NET Core project based on displaying Time Log of users</p>

🔨 Built With
--

- [.NET Core 6.0]
- [ASP.NET CORE 6.0 MVC]

* Database:
  - [Entity Framework Core 6.0]
  - MSSQL Server
* Back-End Language:
  - C#
* Front-End Language:
  - [Angular 17]
* Markup Languages:
  - HTML5
  - CSS3
  - [Bootstrap 5]
* Additionals:
  - Data Seeding
  - AutoMapper
  - google-chart

# License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE) file for details
